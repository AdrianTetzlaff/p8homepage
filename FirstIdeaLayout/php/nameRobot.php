<?php
session_start();
	$thisPage = "nameRobot.php";
	if(!array_key_exists("ip",$_SESSION))
	{
		header("Location: ../index.php");
	}
	else if(strcmp($_SESSION["blockNameRobot"], "true") == 0)
	{
		header("Location:" . $_SESSION["currPage"]);
	}
	else
	{
		$_SESSION["currPage"] = $thisPage;
	} 
?>

<!DOCTYPE html>
<html lang ="en-US">

<html>
	
<head>
	<meta charset="UTF-8">
	<title>Robot Experiment</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="../styles.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>


<div class="container" style="margin-top: 30px;">
	<div class="row text-center" style="margin-bottom: 20px;">
		<div class="col-lg-12" style="font-size: 1.5vw;">
			Please give your robot a name
		</div>
	</div>

	<div class="row h-100 text-center" style="font-size: 1.5vw;">
		<div class="col-lg-6 my-auto">
			<form method="post" action="handleFormData.php" name="nameRobot">
				<input type="hidden" name="whichForm" value="nameRobot">
				<label for="robotName">Enter robot name: </label>
				<input type="text" autocomplete="off" id="robot" name="robotName" placeholder="Name" value="<?php echo $_SESSION['robotname']?>" maxlength="10" pattern="[a-zA-Z0-9]+" minlength="3" required>

		</div>
		<div class="col-lg-6">
			<img id="bigpic" src="../img/temporary/naoFrontal.jpg" style="display:block; align-content: center" />
		</div>

	</div>

	<div class="row text-center" style="margin-top: 20px;">
		<div class="col-2">
			<button type="button" class="btn btn-outline-dark" onclick="window.location.href = 'personaliseIntro.php'">Previous</button>
		</div>
				
		<div class="col-8">
					
		</div>

		<div class="col-2">
			<input type="submit" class="btn btn-outline-dark" id="sub" name="submit" value="Save">
		</div>
			</form>
	</div>
</div>

<div class="container.fluid">
	<div class="row text-center" style="margin-top: 20px;">
		<div class="col-12">
			<?php if(strcmp($_SESSION["admin"], "admin") == 0){?><iframe src="echoSession.php" style="width: 100%; height: 300px;"> </iframe> <head> 	<link rel="stylesheet" type="text/css" href="../stylesAdmin.css"></head> <?php } ?>
		</div>
	</div>
</div>

</body>

</html>