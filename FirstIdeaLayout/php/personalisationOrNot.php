<?php
session_start();
	if(!array_key_exists("ip",$_SESSION))
	{
		header("Location: ../index.php");
	}
	else if(strcmp($_SESSION["admin"], "admin") != 0)
	{
		header("Location:" . $_SESSION["currPage"]);
	}
?>

<!DOCTYPE html>
<html lang ="en-US">

<html>
	
<head>
	<meta charset="UTF-8">
	<title>Robot Experiment</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="../styles.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
	
<div class="container" style="margin-top: 200px;">
	<div class="row text-center">

		<div class="col-2">
					
		</div>

		<div class="col-2">
			<form method="post" action="handleFormData.php">
			<input type="hidden" name="personalisedGroup" value="personalised">
			<input type="hidden" name="whichForm" value="personalisationOrNot">
			<input type="submit" class="btn btn-outline-dark" name="submit" value="Personalised">
			</form>		
		</div>
				
		<div class="col-4">
			
		</div>

		<div class="col-2">
			<form method="post" action="handleFormData.php">
			<input type="hidden" name="personalisedGroup" value="unpersonalised">
			<input type="hidden" name="whichForm" value="personalisationOrNot">
			<input type="submit" class="btn btn-outline-dark" name="submit2" value="Unpersonalised" >
			</form>	
		</div>

		<div class="col-2">
					
		</div>
	</div>

	<div class="row text-center" style="margin-top: 100px;">
		<div class="col-2">
			<button type="button" class="btn btn-outline-dark" onclick="window.location.href = 'introduction.php'">Previous</button>
		</div>
				
		<div class="col-8">
					
		</div>

		<div class="col-2">
		</div>
	</div>

</div>

<div class="container.fluid">
	<div class="row text-center" style="margin-top: 20px;">
		<div class="col-12">
			<?php if(strcmp($_SESSION["admin"], "admin") == 0){?><iframe src="echoSession.php" style="width: 100%; height: 300px;"> </iframe> <head> 	<link rel="stylesheet" type="text/css" href="../stylesAdmin.css"></head> <?php } ?>
		</div>
	</div>
</div>

</body>

</html>