<?php
session_start();
 
	$thisPage = "customiseEyes.php";
	if(!array_key_exists("ip",$_SESSION))
	{
		header("Location: ../index.php");
	}
	else if(strcmp($_SESSION["blockCustomiseEyes"], "true") == 0)
	{
		header("Location:" . $_SESSION["currPage"]);
	}
	else
	{
		if(strcmp($_SESSION["currPage"], $thisPage) != 0)
		{
			$_SESSION["prevPage"] = $_SESSION["currPage"];

		} 
		$_SESSION["currPage"] = $thisPage;
	} 
	?>


<!DOCTYPE html>
<html lang ="en-US">

<html>
	
<head>
	<meta charset="UTF-8">
	<title>Robot Experiment</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="../styles.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function pictureBlueEyes(){ 
	        var pic = "../img/eyes/blueEyes.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: blue;";
        }
        function pictureGreenEyes(){ 
	        var pic = "../img/eyes/greenEyes.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: green;";
        }
        function pictureYellowEyes(){ 
	        var pic = "../img/eyes/yellowEyes.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: yellow;";
        }
        function pictureWhiteEyes(){ 
	        var pic = "../img/eyes/whiteEyes.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: white;";
        }
        function pictureRedEyes(){ 
	        var pic = "../img/eyes/redEyes.jfif";
	        document.getElementById('bigpic').src = pic.replace();
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: red;";
        }
        function pictureBlackOffEyes(){ 
	        var pic = "../img/eyes/blackOffEyes.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: black;";
        }

    </script>


</head>

<body <?php if(strcmp($_SESSION["eyecolorVisited"], "false") == 0){?>onload="pictureBlackOffEyes();"<?php } $_SESSION["eyecolorVisited"] = "true";
 ?>>
	
<div class="container" style="margin-top: 30px;">
	<div class="row text-center" style="margin-bottom: 20px;">
		<div class="col-lg-12" style="font-size: 1.5vw;">
			Which eyecolor should <?php echo $_SESSION["robotname"]?> have?
		</div>
	</div>



	<div class="row h-100 text-center" style="font-size: 1.5vw;">
		<div class="col-lg-6">
<form method="post" action="handleFormData.php" name="customiseEyes">
				<input type="hidden" name="whichForm" value="customiseEyes">
				<div class="row h-100 text-center" style="font-size: 1.5vw;">
					<div class="col-lg-6">
						<input type="submit" name="blue_button" value="Blue" formaction="" class="btn btn-eyesBlue">
					</div>
					<div class="col-lg-6 my-auto">
						<input type="submit" name="green_button" value="Green" formaction="" class="btn btn-eyesGreen">
					</div>
				</div>
				<div class="row h-100 text-center" style="font-size: 1.5vw; padding-top: 20px;">
					<div class="col-lg-6 my-auto">
						<input type="submit" name="yellow_button" value="Yellow" formaction="" class="btn btn-eyesYellow">
					</div>
					<div class="col-lg-6 my-auto">
						<input type="submit" name="white_button" value="White" formaction="" class="btn btn-eyesWhite">
					</div>
				</div>
				<div class="row h-100 text-center" style="font-size: 1.5vw; padding-top: 20px;">
					<div class="col-lg-6 my-auto">
						<input type="submit" name="red_button" value="Red" formaction="" class="btn btn-eyesRed">
					</div>
					<div class="col-lg-6 my-auto">
						<input type="submit" name="black_button" value="Black/Off" formaction="" class="btn btn-eyesBlackOff">
					</div>
				</div>
		</div>
		<div class="col-lg-6" text-center style="margin-bottom: 30px;">


			<section class = "PictureOfEyes">
			 <img id="bigpic" src="bigpic" style="display:none;" />
			</section>
		</div>

	</div>




	<div class="row text-center" style="margin-top: 30px;">
		<div class="col-2">
			<button type="button" class="btn btn-outline-dark" onclick="window.location.href = 'nameRobot.php'">Previous</button>
		</div>
				
		<div class="col-8">
					
		</div>

		<div class="col-2">
			<input type="submit" name="submit" value="Next" class="btn btn-outline-dark">

		</div>
</form>

	</div>
</div>

<div class="container.fluid">
	<div class="row text-center" style="margin-top: 20px;">
		<div class="col-12">
			<?php if(strcmp($_SESSION["admin"], "admin") == 0){?><iframe src="echoSession.php" style="width: 100%; height: 300px;"> </iframe> <head> 	<link rel="stylesheet" type="text/css" href="../stylesAdmin.css"></head> <?php } ?>
		</div>
	</div>
</div>

</body>




<?php 

	if(isset($_POST['blue_button']))
	{
		echo '<script type="text/javascript">', 'pictureBlueEyes();', '</script>';
		$_SESSION["eyecolor"] = "blue";
	}
	else if(isset($_POST['green_button']))
	{
		echo '<script type="text/javascript">', 'pictureGreenEyes();', '</script>';
		$_SESSION["eyecolor"] = "green";
	}
	else if(isset($_POST['yellow_button']))
	{
		echo '<script type="text/javascript">', 'pictureYellowEyes();', '</script>';
		$_SESSION["eyecolor"] = "yellow";
	}
	else if(isset($_POST['white_button']))
	{
		echo '<script type="text/javascript">', 'pictureWhiteEyes();', '</script>';
		$_SESSION["eyecolor"] = "white";

	}
	else if(isset($_POST['red_button']))
	{
		echo '<script type="text/javascript">', 'pictureRedEyes();', '</script>';
		$_SESSION["eyecolor"] = "red";
	}
	else if(isset($_POST['black_button']))
	{
		echo '<script type="text/javascript">', 'pictureBlackOffEyes();', '</script>';
		$_SESSION["eyecolor"] = "blackOff";
	}




		
?>

</html>
