<?php
session_start();
	$thisPage = "finalPersonalisedRobot.php";
	if(!array_key_exists("ip",$_SESSION))
	{
		header("Location: ../index.php");
	}
	else if(strcmp($_SESSION["blockFinalPersonalisedRobot"], "true") == 0)
	{
		header("Location:" . $_SESSION["currPage"]);
	}
	else
	{
		if(strcmp($_SESSION["currPage"], $thisPage) != 0)
		{
			$_SESSION["prevPage"] = $_SESSION["currPage"];

		} 
		$_SESSION["currPage"] = $thisPage;
	} 
?>

<!DOCTYPE html>
<html lang ="en-US">

<html>
	
<head>
	<meta charset="UTF-8">
	<title>Robot Experiment</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="../styles.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>




<div class="container" style="margin-top: 10px;">
	<div class="row">
		<div class="col-lg-2 text-center">
		</div>
				
		<div class="col-lg-8 text-center">
			<p>Thank you for personalising your robot. Below you can see how your robot looks like now. If you want to make changes please use the "previous" buttons to go back and make changes.</p>
			<p>If you are happy, please click on "Save & Continue".</p>
		</div>

		<div class="col-lg-2 text-center">
		</div>
	</div>

	<div class="row text-center">
		<div class="col-3 text-center">
		</div>

		<div class="col-6 text-center">
			<img id="bigpic" src="../img/final/myRobot.jpg" alt="Insert finalised Robot" style="display:block; align-content: center" />
		</div>

		<div class="col-3 text-center">
		</div>
	</div>

	<div class="row text-center" style="margin-bottom: 20px;">
		<div class="col-2">
			<button type="button" class="btn btn-outline-dark" onclick="window.location.href = 'customiseJointProtectors.php'">Previous</button>


		</div>
				
		<div class="col-8">
				


		</div>

		<div class="col-2">
			<form method="post" action="handleFormData.php" name="nameRobot">
				<input type="hidden" name="whichForm" value="finalPersonalisedRobot">
				<input type="submit" class="btn btn-outline-dark" id="sub" name="submit" value="Save & Continue">
			</form>
		</div>
	</div>

</div>


<div class="container.fluid">
	<div class="row text-center" style="margin-top: 20px;">
		<div class="col-12">
			<?php if(strcmp($_SESSION["admin"], "admin") == 0){?><iframe src="echoSession.php" style="width: 100%; height: 300px;"> </iframe> <head> 	<link rel="stylesheet" type="text/css" href="../stylesAdmin.css"></head> <?php } ?>
		</div>
	</div>
</div>


</body>

</html>