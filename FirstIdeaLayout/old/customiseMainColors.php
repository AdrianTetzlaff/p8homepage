<?php
session_start();
	$thisPage = "customiseMainColors.php";
	if(!array_key_exists("ip",$_SESSION))
	{
		header("Location: ../index.php");
	}
	else if(strcmp($_SESSION["blockCustomiseMainColors"], "true") == 0)
	{
		header("Location:" . $_SESSION["currPage"]);
	}
	else
	{
		if(strcmp($_SESSION["currPage"], $thisPage) != 0)
		{
			$_SESSION["prevPage"] = $_SESSION["currPage"];

		} 
		$_SESSION["currPage"] = $thisPage;
	} 
?>


<!DOCTYPE html>
<html lang ="en-US">

<html>
	
<head>
	<meta charset="UTF-8">
	<title>Robot Experiment</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
 	<link rel="stylesheet" type="text/css" href="../styles.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

	<script type="text/javascript">
        function pictureBlueNormalMain(){ 
	        var pic = "../img/mainColor/blueNormalMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: #00a6ff;";
        }

        function pictureGreenNormalMain(){ 
	        var pic = "../img/mainColor/greenNormalMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: #00dd00;";
        }

        function pictureYellowNormalMain(){ 
	        var pic = "../img/mainColor/yellowNormalMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: #f7ff00;";
        }

        function pictureRedNormalMain(){ 
	        var pic = "../img/mainColor/redNormalMain.jfif";
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: #ff0000;";
        }

        function pictureWhiteMain(){ 
	        var pic = "../img/mainColor/whiteMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: #fff;";
        }

        function pictureBlackMain(){ 
	        var pic = "../img/mainColor/blackMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: #000000;";
        } 

        /*
        function pictureBlueDarkMain(){ 
	        var pic = "../img/mainColor/blueDarkMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	        document.getElementById('bigpic').style="background-color: #006ea9;";
        }


        function pictureGreenDarkMain(){ 
	        var pic = "../img/mainColor/greenDarkMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: #009200;";
        }


        function pictureYellowDarkMain(){ 
	        var pic = "../img/mainColor/yellowDarkMain.jfif"
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: #a4a900;";
        }

        function pictureRedDarkMain(){ 
	        var pic = "../img/mainColor/redDarkMain.jfif";
	        document.getElementById('bigpic').src = pic.replace('90x90', '225x225');
	        document.getElementById('bigpic').style.display='block';
	       	document.getElementById('bigpic').style="background-color: #8e2913;";
        }*/


	</script>


</head>

<body <?php if(strcmp($_SESSION["maincolorVisited"], "false") == 0){?>onload="pictureWhiteMain();"><?php } $_SESSION["maincolorVisited"] = "true";
 ?>>
	
<div class="container" style="margin-top: 30px;">
	<div class="row text-center" style="margin-bottom: 20px;">
		<div class="col-lg-12" style="font-size: 1.5vw;">
			Which color should <?php echo $_SESSION["robotname"]?>'s body have?
		</div>
	</div>



	<div class="row h-100 text-center" style="font-size: 1.5vw;">
		<div class="col-lg-6">
<form method="post" action="handleFormData.php" name="customiseMainColor">
				<input type="hidden" name="whichForm" value="customiseMainColor">
				<div class="row h-100 text-center" style="font-size: 1.5vw;">
					<div class="col-lg-6 my-auto">
						<input type="submit" name="blueNormal_button" value="Blue" formaction="" class="btn btn-normalBlue">
					</div>

					<div class="col-lg-6 my-auto">
						<input type="submit" name="greenNormal_button" value="Green" formaction="" class="btn btn-normalGreen">
					</div>
				</div>

				<div class="row h-100 text-center" style="font-size: 1.5vw; padding-top: 20px;">
					<div class="col-lg-6 my-auto">
						<input type="submit" name="yellowNormal_button" value="Yellow" formaction="" class="btn btn-normalYellow">
					</div>				
					<div class="col-lg-6 my-auto">
						<input type="submit" name="redNormal_button" value="Red" formaction="" class="btn btn-normalRed">
					</div>
				</div>

				<div class="row h-100 text-center" style="font-size: 1.5vw; padding-top: 20px;">
					<div class="col-lg-6 my-auto">
						<input type="submit" name="white_button" value="White" formaction="" class="btn btn-white">
					</div>
					<div class="col-lg-6 my-auto">
						<input type="submit" name="black_button" value="Black" formaction="" class="btn btn-black">
					</div>	
				</div>				
				
<!--
					<div class="col-lg-4 my-auto">
						<input type="submit" name="greenDark_button" value="Dark Green" formaction="" class="btn btn-darkGreen">
					</div>
					<div class="col-lg-4 my-auto">
						<input type="submit" name="blueDark_button" value="Dark Blue" formaction="" class="btn btn-darkBlue">
					</div>
					<div class="col-lg-4 my-auto">
						<input type="submit" name="yellowDark_button" value="Dark Yellow" formaction="" class="btn btn-darkYellow">
					</div>
					<div class="col-lg-4 my-auto">
						<input type="submit" name="redDark_button" value="Dark Red" formaction="" class="btn btn-darkRed">
					</div>
-->

		</div>
		
		<div class="col-lg-6" text-center>


			<section class = "PictureOfEyes">
			 <img id="bigpic" src="bigpic" style="display:none;" />
			</section>
		</div>

	</div>




	<div class="row text-center" style="margin-top: 30px;">
		<div class="col-2">
			<button type="button" class="btn btn-outline-dark" onclick="window.location.href = 'customiseEyes.php'">Previous</button>
		</div>
				
		<div class="col-8">
					
		</div>

		<div class="col-2">
			<input type="submit" name="submit" value="Next" class="btn btn-outline-dark">

		</div>
</form>

	</div>
</div>

<div class="container.fluid">
	<div class="row text-center" style="margin-top: 20px;">
		<div class="col-12">
			<?php if(strcmp($_SESSION["admin"], "admin") == 0){?><iframe src="echoSession.php" style="width: 100%; height: 300px;"> </iframe> <head> 	<link rel="stylesheet" type="text/css" href="../stylesAdmin.css"></head> <?php } ?>
		</div>
	</div>
</div>

</body>




<?php 

	if(isset($_POST['blueNormal_button']))
	{
		echo '<script type="text/javascript">', 'pictureBlueNormalMain();', '</script>';
		$_SESSION["maincolor"] = "blueNormal";
	}
	if(isset($_POST['blueDark_button']))
	{
		echo '<script type="text/javascript">', 'pictureBlueDarkMain();', '</script>';
		$_SESSION["maincolor"] = "blueDark";
	}

	else if(isset($_POST['greenNormal_button']))
	{
		echo '<script type="text/javascript">', 'pictureGreenNormalMain();', '</script>';
		$_SESSION["maincolor"] = "greenNormal";
	}
	else if(isset($_POST['greenDark_button']))
	{
		echo '<script type="text/javascript">', 'pictureGreenDarkMain();', '</script>';
		$_SESSION["maincolor"] = "greenDark";
	}

	else if(isset($_POST['yellowNormal_button']))
	{
		echo '<script type="text/javascript">', 'pictureYellowNormalMain();', '</script>';
		$_SESSION["maincolor"] = "yellowNormal";
	}
	else if(isset($_POST['yellowDark_button']))
	{
		echo '<script type="text/javascript">', 'pictureYellowDarkMain();', '</script>';
		$_SESSION["maincolor"] = "yellowDark";
	}

	else if(isset($_POST['white_button']))
	{
		echo '<script type="text/javascript">', 'pictureWhiteMain();', '</script>';
		$_SESSION["maincolor"] = "white";

	}
	else if(isset($_POST['black_button']))
	{
		echo '<script type="text/javascript">', 'pictureBlackMain();', '</script>';
		$_SESSION["maincolor"] = "black";
	}

	else if(isset($_POST['redNormal_button']))
	{
		echo '<script type="text/javascript">', 'pictureRedNormalMain();', '</script>';
		$_SESSION["maincolor"] = "redNormal";
	}
	else if(isset($_POST['redDark_button']))
	{
		echo '<script type="text/javascript">', 'pictureRedDarkMain();', '</script>';
		$_SESSION["maincolor"] = "redDark";
	}





		
?>

</html>
